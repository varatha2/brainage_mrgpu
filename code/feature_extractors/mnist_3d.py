'''Sample feature extractor for sMRI images based on MNIST example.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import tensorflow as tf


# Create some wrappers for simplicity

def conv3d(x, W, b, strides=1):
	''' Conv3D wrapper with bias and relu activation.'''

	x = tf.nn.conv3d(x, W, strides=[1, strides, strides, strides, 1], padding='SAME')
	x = tf.nn.bias_add(x, b)
	return tf.nn.relu(x)

def maxpool3d(x, k=2):
	''' MaxPool3D wrapper.'''

	return tf.nn.max_pool3d(x, ksize=[1, k, k, k, 1], strides=[1, k, k, k, 1], padding='SAME')

class MNIST3D(object):
	'''Class that defines MNIST 3D feature extractor.'''

	def __init__(self, crop_shape, used_dtype=tf.float32):
		self._input_shape = crop_shape
		self._dtype = used_dtype

	def conv_net(self, x, weights, biases, dropout):
		'''The Conv Net model.'''

	# # Select GPU
	# with tf.device('/device:GPU:1'):

		# Tensor inputs become 5-D: [Batch size, depth, Height, Width, Channel]
		x = tf.reshape(x, shape=[-1, self._input_shape[0], self._input_shape[1], self._input_shape[2], 1])

		# Convolution Layer
		conv1 = conv3d(x, weights['wc1'], biases['bc1'])
		# Max Pooling
		conv1 = maxpool3d(conv1, k=2)

	# # Select GPU
	# with tf.device('/device:GPU:1'):

		# Convolution Layer
		conv2 = conv3d(conv1, weights['wc2'], biases['bc2'])
		# Max Pooling
		conv2 = maxpool3d(conv2, k=2)

		# Fully Connected Layer
		fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
		fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
		fc1 = tf.nn.relu(fc1)

		# Apply Dropout
		fc1 = tf.nn.dropout(fc1, dropout)

		# Ouput, class prediction
		out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])

		return out


	def init_weights(self):
		''' Initialize weights and biases.'''

		weights = {
			# 5x5 conv, 1 input, 32 outputs
			'wc1': tf.Variable(tf.random_normal([5, 5, 5, 1, 32], dtype=self._dtype)),
			# 5x5 conv, 32 inputs, 64 outputs
			'wc2': tf.Variable(tf.random_normal([5, 5, 5, 32, 64], dtype=self._dtype)),
			# fully connected, a*b*c*64 inputs, 256 outputs
			'wd1': tf.Variable(
				tf.random_normal([self._input_shape[0]*self._input_shape[1]*self._input_shape[2], 256],
								 dtype=self._dtype)),
			# 256 inputs, 1- outputs (class prediction)
			'out': tf.Variable(tf.random_normal([256, 1], dtype=self._dtype))
		}

		biases = {
			'bc1': tf.Variable(tf.random_normal([32], dtype=self._dtype)),
			'bc2': tf.Variable(tf.random_normal([64], dtype=self._dtype)),
			'bd1': tf.Variable(tf.random_normal([256], dtype=self._dtype)),
			'out': tf.Variable(tf.random_normal([1], dtype=self._dtype))
		}

		return weights, biases