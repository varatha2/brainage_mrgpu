'''Sample feature extractor for RAM spectrograms based on MNIST example.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import tensorflow as tf


# Create some wrappers for simplicity

def conv2d(x, W, b, strides=1):
	''' Conv2D wrapper with bias and relu activation.'''

	x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
	x = tf.nn.bias_add(x, b)
	return tf.nn.relu(x)

def maxpool2d(x, k=2):
	''' MaxPool2D wrapper.'''

	return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')


class MNIST2D(object):
	'''Class that defines MNIST 2D feature extractor.'''

	def __init__(self, input_shape, num_channels, used_dtype=tf.float32):
		self._input_shape = input_shape
		self._num_channels = num_channels
		self._dtype = used_dtype
		self._weights = None
		self._biases = None

	def conv_net(self, x, dropout):
		'''The Conv Net model.'''

	# # Select GPU
	# with tf.device('/device:GPU:1'):

		# Tensor inputs become 5-D: [Batch size, depth, Height, Channel]
		x = tf.reshape(x, shape=[-1, self._input_shape[0], self._input_shape[1], self._num_channels])

		# Convolution Layer
		conv1 = conv2d(x, self._weights['wc1'], self._biases['bc1'])
		# Max Pooling
		conv1 = maxpool2d(conv1, k=2)

	# # Select GPU
	# with tf.device('/device:GPU:1'):

		# Convolution Layer
		conv2 = conv2d(conv1, self._weights['wc2'], self._biases['bc2'])
		# Max Pooling
		conv2 = maxpool2d(conv2, k=2)

		# Fully Connected Layer
		fc1 = tf.reshape(conv2, [-1, self._weights['wd1'].get_shape().as_list()[0]])
		fc1 = tf.add(tf.matmul(fc1, self._weights['wd1']), self._biases['bd1'])
		fc1 = tf.nn.relu(fc1)

		# Apply Dropout
		fc1 = tf.nn.dropout(fc1, dropout)

		# Ouput, class prediction
		out = tf.add(tf.matmul(fc1, self._weights['out']), self._biases['out'])

		return out


	def init_weights(self):
		''' Initialize weights and biases.'''

		self._weights = {
			# 5x5 conv, n input channels, 32 outputs
			'wc1': tf.Variable(tf.random_normal([5, 5, self._num_channels, 32], dtype=self._dtype)),
			# 5x5 conv, 32 inputs, 64 outputs
			'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64], dtype=self._dtype)),
			# fully connected, a*b*64 inputs, 256 outputs
			'wd1': tf.Variable(
				tf.random_normal([self._input_shape[0]*self._input_shape[1]*64 // 16, 256],
								 dtype=self._dtype)),
			# 256 inputs, 1- outputs (class prediction)
			'out': tf.Variable(tf.random_normal([256, 1], dtype=self._dtype))
		}

		self._biases = {
			'bc1': tf.Variable(tf.random_normal([32], dtype=self._dtype)),
			'bc2': tf.Variable(tf.random_normal([64], dtype=self._dtype)),
			'bd1': tf.Variable(tf.random_normal([256], dtype=self._dtype)),
			'out': tf.Variable(tf.random_normal([1], dtype=self._dtype))
		}