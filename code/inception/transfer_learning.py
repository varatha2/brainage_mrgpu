import numpy as np
import os
import tensorflow as tf
# from matplotlib import pyplot as plt
# from datasets import imagenet
from nets import inception
from preprocessing import inception_preprocessing
from tensorflow.contrib import slim
from datasets import dataset_utils

def decode_image_from_filename(filename):
    print(tf.read_file(filename))
    return tf.image.decode_jpeg(tf.read_file(filename), channels=3)

def generate_batch():
    return None

if __name__ == '__main__':

    image_filename = 'dog.jpg'
    # checkpoints_dir = 'checkpoints'
    checkpoints_dir = '/home/varatha2/projects/brain_age/code/inception/checkpoints'
    image_size = 128 # inception.inception_v1.default_image_size # 100

    with tf.Graph().as_default():
        image = decode_image_from_filename(image_filename)
        print('image shape', image.get_shape(), image.shape)
       
        # Create placeholder 
        image_placeholder = tf.placeholder(tf.float32, shape=(None, image_size, image_size, 3), name='image_placeholder')

        # Create batch input
        processed_image = inception_preprocessing.preprocess_image(image, image_size, image_size, is_training=False)
        processed_images = tf.expand_dims(processed_image, 0)
        processed_images = tf.stack([processed_image, processed_image])
        print(processed_images.get_shape()) 
        
        # Create the model, use the default arg scope to configure the batch norm parameters.
        with slim.arg_scope(inception.inception_v1_arg_scope()):
            net, end_points = inception.inception_v1_base(image_placeholder)
        print('inception base shape', net.get_shape())

        # Inception -> fc -> 1 neuron
        avg_pool_kernel_size = net.get_shape()[1:-1]
        print('avg_pool kernel size', avg_pool_kernel_size)
        net = tf.contrib.layers.avg_pool2d(net, avg_pool_kernel_size, stride=1)
        net = tf.squeeze(net, [1, 2], name='squeeze')
        print('after squeeze', net.get_shape())
        logits = tf.contrib.layers.fully_connected(net, num_outputs=1)
        print('after fully connected', logits.get_shape())

        # Specify the loss function:
        labels = np.array([1, 1], dtype=np.float32).reshape([2, -1])
        loss_op = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=labels)
       
        # Specify the optimizer and create the train op:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.0000000001)
        train_op = optimizer.minimize(loss_op)

        # Load the checkpoint
        # print('Inception variables ', slim.get_model_variables('InceptionV1'))
        # print()
        # print('Variables to restore', slim.get_variables_to_restore())
        init_fn = slim.assign_from_checkpoint_fn(
            os.path.join(checkpoints_dir, 'inception_v1.ckpt'),
            slim.get_model_variables('InceptionV1'))
        
        with tf.Session() as sess:
            batch = processed_images.eval() # np.ones([1, image_size, image_size, 3], dtype=np.float32) # processed_images.eval()
            print('batch type', type(batch), 'shape', batch.shape)
            
            init_fn(sess)
            init = tf.global_variables_initializer()
            sess.run(init, feed_dict={image_placeholder: batch})

            loss_history = np.ndarray(100)
            for i in range(100):
                sess.run(train_op, feed_dict={image_placeholder: batch})
                loss = sess.run(loss_op, feed_dict={image_placeholder: batch})
                loss_history[i] = loss[0]
                print(i, loss)
       
        # plt.figure()
        # plt.plot(loss_history)
        # plt.show()

