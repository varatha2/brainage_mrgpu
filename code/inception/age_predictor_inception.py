'''Sample age predictor script using inception v1 pretrained on the ILSVRC-2012-CLS image classification dataset.
   In order to run this file, follow instructions on downloading tf slim from https://github.com/tensorflow/models/tree/master/research/slim
   and add the folder to the python path.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import numpy as np
from sklearn.metrics import roc_auc_score
import tensorflow as tf
from tensorflow.app import flags
from ..utils import data_pipeline
from ..feature_extractors import mnist_3d
from nets import inception
from tensorflow.contrib import slim 
import os

FLAGS = flags.FLAGS

flags.DEFINE_integer('crop_shape', 128, 'Crop shape of one dimension, the same shape will be used for all dimensions.')
flags.DEFINE_integer('batch_size', 4, 'Number of input images in a batch.')
flags.DEFINE_boolean('use_fp16', False, 'Boolean indicating whether float16 precision is used.')

# Training parameters
learning_rate = 0.001
num_steps = 50000
display_step = 100

# Network parameters
num_classes = 1
dropout = 0.6

########################################################
# Currently tf.float16 functionality is NOT supported. #
########################################################

def main(unused_argv):

	used_dtype = tf.float16 if FLAGS.use_fp16 else tf.float32

	# Select CPU
	with tf.device('/device:CPU:0'):
		# Data
		# Training batch generation
		# images, labels = data_pipeline.generate_batch_input('code/test_data/test_tfrecords', 'Subject/gender/value',
		# 													[64, 64, 64], 5, 199, True)
		tr_images, tr_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_1.5T_Yr1_all_2mm',
			'Subject/age/value', [FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape],
			FLAGS.batch_size, 2292, train=True, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)

		# Unflatten training images 
		tr_images = tf.manip.reshape(tr_images, (-1, FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape))
		mid = FLAGS.crop_shape // 2
		tr_images = tr_images[:,:,:,mid-1:mid+2]

		# Testing batch generation
		ts_images, ts_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_BL_3T_all_2mm',
			'Subject/age/value', [FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape],
			FLAGS.batch_size, 199, train=False, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)
		
		# Unflatten testing images
		ts_images = tf.manip.reshape(ts_images, (-1, FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape))
		ts_images = ts_images[:,:,:,mid-1:mid+2]

		X = tf.placeholder(used_dtype, [None, FLAGS.crop_shape, FLAGS.crop_shape, 3])
		Y = tf.placeholder(used_dtype, [None, num_classes])
		# keep_prob = tf.placeholder(used_dtype)

	# -> this is the location to import the model
	# Select GPU
	with tf.device('/device:GPU:0'):
		# Construct model
		with slim.arg_scope(inception.inception_v1_arg_scope()):
			net, end_points = inception.inception_v1_base(X)

	        # Inception -> fc -> 1 neuron
		avg_pool_kernel_size = net.get_shape()[1:-1]
		net = tf.contrib.layers.avg_pool2d(net, avg_pool_kernel_size, stride=1)
		net = tf.squeeze(net, [1, 2], name='squeeze')
		logits = tf.contrib.layers.fully_connected(net, num_outputs=num_classes)
	
		# Get function to restore inception weights
		checkpoints_dir = '/home/varatha2/projects/brain_age/code/inception/checkpoints'
		init_fn = slim.assign_from_checkpoint_fn(
			os.path.join(checkpoints_dir, 'inception_v1.ckpt'),
			slim.get_model_variables('InceptionV1'))
 			

	# Select GPU
	with tf.device('/device:GPU:1'):
		# use mse for regression
		prediction = logits
		loss_op = tf.losses.mean_squared_error(labels=Y, predictions=prediction)

		# optimizer = tf.train.AdamOptimizer(learning_rate=1e-3, epsilon=1e-4)
		optimizer = tf.train.AdamOptimizer(learning_rate=1e-5, epsilon=1e-4)
		train_op = optimizer.minimize(loss_op)

		# Evaluate model
		# correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
		# accuracy = tf.reduce_mean(tf.cast(correct_pred, used_dtype))
		# auc_op = tf.metrics.auc(Y, prediction)

	config = tf.ConfigProto(allow_soft_placement=True)
	config.gpu_options.allow_growth = True

	# Start training
	with tf.Session(config=config) as sess:

		# Initialize the variables (i.e. assign their default value)
		init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
		# Run the initializer
		sess.run(init)
		
		# Restore pretrained inception weights
		init_fn(sess)

		# Create a coordinator and run all QueueRunner objects
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		num_test_batches = 199 // FLAGS.batch_size

		test_labels = np.zeros((num_test_batches, FLAGS.batch_size))
		test_preds = np.zeros((num_test_batches, FLAGS.batch_size))

		for step in range(1, num_steps):
			# Run optimization op (backprop)
			tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
			# print('Batch input :' + str(tr_imgs.shape))
			sess.run(train_op, feed_dict={X: tr_imgs, Y: tr_lbls})

			# Print information about training progress
			if not step % display_step:
				loss = 0
				for batch in range(0, num_test_batches):
					ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
					mini_loss = sess.run(loss_op, feed_dict={X: ts_imgs, Y: ts_lbls})
					loss += mini_loss

				print("Step " + str(step) + ", Test Loss= " +
					  "{:.4f}".format(loss / num_test_batches))

		print("Optimization Finished!")


		# Final test loss calculation
		loss = 0
		for batch in range(0, num_test_batches):
			ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
			mini_loss = sess.run(loss_op, feed_dict={X: ts_imgs, Y: ts_lbls})
			loss += mini_loss
		
		print('Final test loss= ' + '{:.3f}'.format(loss / num_test_batches))

		# Stop the threads
		coord.request_stop()

		# Wait for threads to stop
		coord.join(threads)
		sess.close()

if __name__ == '__main__':
	tf.app.run()
