'''A head that performs classification using a predefined feature extractor.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import tensorflow as tf


class ClassificationHead(object):
    '''Class that defines a classification head.'''

    def __init__(self, pbtxt):
        '''
        Args:
            pbtxt: protocol buffer object with model information.
        '''
        self._num_classes = pbtxt.head.classification.num_classes
        self._hidden_layers = None
        if pbtxt.head.hidden_layers:
            self._hidden_layers = pbtxt.head.hidden_layers
        self._optimizer = pbtxt.training.adam
        self._epsilon = pbtxt.training.adam.epsilon
        self._learning_rate = pbtxt.training.adam.learning_rate
        self._dropout_keep_prob = pbtxt.dropout_keep_prob

    def get_logits(self, feature_map, is_training):
        '''Computes logits for feature maps.

        Args:
            feature_map: feature map generated by convolutional layers.
            is_training: boolean indicating whether the model is training
        Returns:
            logits: output of the final fully connected layer.
        '''

        if self._hidden_layers:
            for h in self._hidden_layers:
                feature_map = tf.contrib.layers.fully_connected(feature_map, h)
                # Apply dropout
                feature_map = tf.contrib.layers.dropout(feature_map, keep_prob=self._dropout_keep_prob,
                                                        is_training=is_training)

        if self._num_classes == 2:
            logits = tf.contrib.layers.fully_connected(feature_map, num_outputs=1)
        else:
            logits = tf.contrib.layers.fully_connected(feature_map, num_outputs=self._num_classes)

        return logits

    def get_predictions(self, logits):
        '''Computes final predictions for logits.
        Args:
            logits: output of the final fully connected layer.
        Returns:
            predictions: final predictions.
        '''

        if self._num_classes == 2:
            predictions = tf.nn.sigmoid(logits)
        else:
            predictions = tf.nn.softmax(logits)

        return predictions

    def get_trainop(self, logits, true_labels):
        '''Returns a training op to perform.
        Args:
            logits: output of the final fully connected layer.
            true_labels: ground truth labels for the current input images.
        Returns:
            train_op: training op in the tf graph.
        '''

        # Define loss and optimizer
        if self._num_classes == 2:
            loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                logits=logits, labels=true_labels))
        else:
            loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
                logits=logits, labels=true_labels))

        optimizer = tf.train.AdamOptimizer(learning_rate=self._learning_rate, epsilon=self._epsilon)
        train_op = optimizer.minimize(loss_op)

        return train_op

