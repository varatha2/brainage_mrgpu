'''This script reads a specified model proto file and runs the model based on the specifications.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import


from ..proto import pipeline_pb2
from google.protobuf import text_format
import tensorflow as tf
from tensorflow import flags
from ..utils import data_pipeline
from ..feature_extractors import pretrained_inception_v1_2d
from ..feature_extractors import patch_based_3D
from ..feature_extractors import pretrained_inception_i3d
from ..heads import classification_head
from ..heads import regression_head
from .args import *
import os
import numpy as np
from sklearn.metrics import roc_auc_score, accuracy_score, mean_absolute_error, mean_squared_error

# FLAGS = flags.FLAGS
#
# flags.DEFINE_string('model_proto', '', 'Absolute path to the model specification proto file.')

def create_summary_dirs(pbtxt):
	'''Creates train and test summary directories if they do not exist.
	Args:
		pbtxt: model proto
	'''

	train_dir = os.path.join(pbtxt.experiment_basedir, 'tfsummary/train')
	test_dir = os.path.join(pbtxt.experiment_basedir, 'tfsummary/test')
	
	if tf.gfile.Exists(train_dir):
		tf.gfile.DeleteRecursively(train_dir)

	if tf.gfile.Exists(test_dir):
		tf.gfile.DeleteRecursively(test_dir)

	tf.gfile.MakeDirs(train_dir)
	tf.gfile.MakeDirs(test_dir)

	return train_dir, test_dir


def main(unused_argv):

	## Read and parse model proto file.
	GetArgs()
	pbtxt = pipeline_pb2.Pipeline()
	with open(GlobalOpts.model_proto, 'r') as f:
		text_format.Parse(f.read(), pbtxt)

	num_classes = 1
	label_key = ''
	if pbtxt.head.HasField('classification'):
		label_key = pbtxt.head.classification.label
		num_classes = 1 if pbtxt.head.classification.num_classes == 2 else pbtxt.head.classification.num_classes
	if pbtxt.head.HasField('regression'):
		label_key = pbtxt.head.regression.label
		num_classes = 1

	used_dtype = tf.float32

	## Create summary directories
	train_dir, test_dir = create_summary_dirs(pbtxt)

	## Data pipeline

	# Select CPU
	with tf.device('/device:CPU:0'):
		# Data
		# Training batch generation.
		tr_images, tr_labels = data_pipeline.generate_batch_input_parallel(
			pbtxt.training.data, label_key, [pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1], pbtxt.preprocessing.crop[2]],
			pbtxt.batch_size, pbtxt.training.num_examples, train=True, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)


		# Testing batch generation.
		ts_images, ts_labels = data_pipeline.generate_batch_input_parallel(
			pbtxt.testing.data, label_key, [pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1], pbtxt.preprocessing.crop[2]],
			pbtxt.batch_size, pbtxt.testing.num_examples, train=False, used_dtype=tf.float32,
			num_preprocess_threads=8, num_readers=8)

		# Convert labels to one-hot format for multi-class classifications.
		if num_classes > 2:
			tr_labels = tf.squeeze(tf.one_hot(indices=tf.cast(tr_labels, tf.int32), depth=num_classes))
			ts_labels = tf.squeeze(tf.one_hot(indices=tf.cast(ts_labels, tf.int32), depth=num_classes))

		# Choose middle 3 slices in the last dimension as RGB channels for a 2D feature extractor.
		if not pbtxt.feature_extractor.ThreeD:
			# De-flatten images.
			tr_images = tf.reshape(tr_images, (-1, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											   pbtxt.preprocessing.crop[2]))
			ts_images = tf.reshape(ts_images, (-1, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											   pbtxt.preprocessing.crop[2]))
			mid = pbtxt.preprocessing.crop[2] // 2
			tr_images = tr_images[:, :, :, mid - 1: mid + 2]
			ts_images = ts_images[:, :, :, mid - 1 : mid + 2]
		else:
			# De-flatten images.
			tr_images = tf.reshape(tr_images, (-1, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											   pbtxt.preprocessing.crop[2], 1))
			ts_images = tf.reshape(ts_images, (-1, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											   pbtxt.preprocessing.crop[2], 1))

		if pbtxt.feature_extractor.conv_net == 'pretrained_inception_I3D':
			# Replicate images to 3 channels.
			tr_images = tf.tile(tr_images, [1, 1, 1, 1, 3])
			ts_images = tf.tile(ts_images, [1, 1, 1, 1, 3])

		# Place holders for input images and labels.
		if not pbtxt.feature_extractor.ThreeD:
			X = tf.placeholder(used_dtype, [None, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1], 3])
		elif not pbtxt.feature_extractor.conv_net == 'pretrained_inception_I3D':
			X = tf.placeholder(used_dtype, [None, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											pbtxt.preprocessing.crop[2], 1])
		else:
			X = tf.placeholder(used_dtype, [None, pbtxt.preprocessing.crop[0], pbtxt.preprocessing.crop[1],
											pbtxt.preprocessing.crop[2], 3])
		Y = tf.placeholder(used_dtype, [None, num_classes])

		is_training = tf.placeholder(tf.bool)  # boolean for if the model is training or not


	## Graph generation

	# Select GPU 0
	with tf.device('/device:GPU:0'):
		# Construct model
		if pbtxt.feature_extractor.conv_net == 'pretrained_inception_2D':
			feature_extractor = pretrained_inception_v1_2d.PretrainedInceptionV1_2D([pbtxt.preprocessing.crop[0],
																				 pbtxt.preprocessing.crop[1]],
																				3, used_dtype)
		elif pbtxt.feature_extractor.conv_net == 'patch_based_3D':
			feature_extractor = patch_based_3D.PatchBased3D([pbtxt.preprocessing.crop[0],
															 pbtxt.preprocessing.crop[1],
															 pbtxt.preprocessing.crop[2]],
															3, used_dtype)
		elif pbtxt.feature_extractor.conv_net == 'pretrained_inception_I3D':
			feature_extractor = pretrained_inception_i3d.PretrainedInceptionI3D([pbtxt.preprocessing.crop[0],
																				 pbtxt.preprocessing.crop[1],
																				 pbtxt.preprocessing.crop[2]],
																				3, used_dtype)

		feature_maps = feature_extractor.conv_net(X, is_training)

		if pbtxt.head.HasField('classification'):
			head_obj = classification_head.ClassificationHead(pbtxt)
		elif pbtxt.head.HasField('regression'):
			head_obj = regression_head.RegressionHead(pbtxt)

		logits = head_obj.get_logits(feature_maps, is_training)


	# Select GPU 1
	with tf.device('/device:GPU:1'):

		# Define training op
		train_op = head_obj.get_trainop(logits=logits, true_labels=Y)

		# Get predictions.
		predictions = tf.reshape(head_obj.get_predictions(logits), [pbtxt.batch_size, num_classes])

		# Tensorboard summary of metrics.
		metric_ops = []

		# for metric in pbtxt.metrics:
		# 	if metric == 'accuracy':
		# 		accuracy, accuracy_op = tf.metrics.accuracy(Y, tf.round(predictions))
		# 		tf.summary.scalar('accuracy', accuracy)
		# 		metric_ops.append(accuracy_op)
		#
		# 	if metric == 'auc':
		# 		auc, auc_op = tf.metrics.auc(Y, predictions)
		# 		tf.summary.scalar('AUC', auc)
		# 		metric_ops.append(auc_op)
		#
		# 	if metric == 'mae':
		# 		mae, mae_op = tf.metrics.mean_absolute_error(Y, predictions)
		# 		tf.summary.scalar('MAE', mae)
		# 		metric_ops.append(mae_op)

		for metric in pbtxt.metrics:
			if metric == 'accuracy':
				accuracy_value_ = tf.placeholder(tf.float32, shape=())
				accuracy_summary = tf.summary.scalar('accuracy', accuracy_value_)

			if metric == 'auc':
				auc_value_ = tf.placeholder(tf.float32, shape=())
				auc_summary = tf.summary.scalar('auc', auc_value_)

			if metric == 'mae':
				mae_value_ = tf.placeholder(tf.float32, shape=())
				mae_summary = tf.summary.scalar('mae', mae_value_)

		summary_merged = tf.summary.merge_all()

	## TF Session

	config = tf.ConfigProto(allow_soft_placement=True)
	config.gpu_options.allow_growth = True

	# Start training
	with tf.Session(config=config) as sess:

		# Initiate summry writers
		train_writer = tf.summary.FileWriter(train_dir, sess.graph)
		test_writer = tf.summary.FileWriter(test_dir)

		init_fn = feature_extractor.init_weights()

		# Initialize the variables (i.e. assign their default value)
		init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
		# Run the initializer
		sess.run(init)

		# Restore pretrained inception weights
		init_fn(sess)

		# Create a coordinator and run all QueueRunner objects
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		num_test_batches = pbtxt.testing.num_examples // pbtxt.batch_size

		train_labels = np.zeros((num_test_batches, pbtxt.batch_size))
		train_preds = np.zeros((num_test_batches, pbtxt.batch_size))

		test_labels = np.zeros((num_test_batches, pbtxt.batch_size))
		test_preds = np.zeros((num_test_batches, pbtxt.batch_size))

		for step in range(1, pbtxt.training.steps):
			# # Run optimization op (backprop)
			# tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
			# # summary, _, _, _ = sess.run([merged, train_op] + metric_ops, feed_dict={X: tr_imgs, Y: tr_lbls})
			# sess.run([train_op] + metric_ops, feed_dict={X: tr_imgs, Y: tr_lbls, is_training: True})
			# summary_tr = sess.run(summary_merged)
			# train_writer.add_summary(summary_tr, step)
			#
			# if not step % 100:
			# # 	for batch in range(0, num_test_batches):
			# 	ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
			# 	# summary, _, _ = sess.run([merged] + metric_ops, feed_dict={X: ts_imgs, Y: ts_lbls})
			# 	sess.run(metric_ops, feed_dict={X: ts_imgs, Y: ts_lbls, is_training: False})
			# 	summary_ts = sess.run(summary_merged)
			#
			# 	test_writer.add_summary(summary_ts, step)

			# Run optimization op (backprop)
			tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
			sess.run(train_op, feed_dict={X: tr_imgs, Y: tr_lbls, is_training: True})

			if not step % 100:
				for batch in range(0, num_test_batches):
					tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
					pred = sess.run(predictions, feed_dict={X: tr_imgs, Y: tr_lbls, is_training: False})
					train_labels[batch, :] = np.reshape(tr_lbls, [-1])
					train_preds[batch, :] = np.reshape(pred, [-1])

					ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
					pred = sess.run(predictions, feed_dict={X: ts_imgs, Y: ts_lbls, is_training: False})
					test_labels[batch, :] = np.reshape(ts_lbls, [-1])
					test_preds[batch, :] = np.reshape(pred, [-1])

				feed_dict = {}
				for metric in pbtxt.metrics:
					if metric == 'accuracy':
						acc = accuracy_score(np.reshape(train_labels, [-1]),
											 np.around(np.reshape(train_preds, [-1])))
						feed_dict.update({accuracy_value_: acc})

					if metric == 'auc':
						auc = roc_auc_score(np.reshape(train_labels, [-1]), np.reshape(train_preds, [-1]))
						feed_dict.update({auc_value_: auc})

					if metric == 'mae':
						mae = mean_absolute_error(np.reshape(train_labels, [-1]), np.reshape(train_preds, [-1]))
						feed_dict.update({mae_value_: mae})

				summary_tr = sess.run(summary_merged, feed_dict=feed_dict)
				train_writer.add_summary(summary_tr, step)

				feed_dict = {}
				for metric in pbtxt.metrics:
					if metric == 'accuracy':
						acc = accuracy_score(np.reshape(test_labels, [-1]),
											 np.around(np.reshape(test_preds, [-1])))
						feed_dict.update({accuracy_value_: acc})

					if metric == 'auc':
						auc = roc_auc_score(np.reshape(test_labels, [-1]), np.reshape(test_preds, [-1]))
						feed_dict.update({auc_value_: auc})

					if metric == 'mae':
						mae = mean_absolute_error(np.reshape(test_labels, [-1]), np.reshape(test_preds, [-1]))
						feed_dict.update({mae_value_: mae})

				summary_ts = sess.run(summary_merged, feed_dict=feed_dict)
				test_writer.add_summary(summary_ts, step)


		print("Optimization Finished!")

		# Stop the threads
		coord.request_stop()

		# Wait for threads to stop
		coord.join(threads)
		sess.close()


if __name__ == '__main__':
	main(0)
