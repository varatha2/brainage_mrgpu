'''Util for creating and reading TF Examples based on RAM word examples.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf

def _bytes_feature(value):
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _floatlist_feature(value):
	return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def _int64_feature(value):
	return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def create_tf_example(subject_id, word_id, session_number, remembered, vectorized_features):
	''' Creates a TF Example using image and additional features.'''

	feature = {
	'subject/ID': _bytes_feature(subject_id.encode()),
	'word/ID': _bytes_feature(word_id.encode()),
	'session/number': _int64_feature(session_number),
	'word/remembered/label': _int64_feature(remembered),
	'spectrogram/regional': _bytes_feature(tf.compat.as_bytes(vectorized_features.tostring()))
	}

	# Return an example protocol buffer.
	return tf.train.Example(features=tf.train.Features(feature=feature))

def parse_tf_example(serialized_adni_example):
	''' Parses an ADNI TF Example and returns image and additional features.
	(Should be run within a TF session.)
	'''

	feature = {
	'subject/ID': tf.FixedLenFeature([], tf.string),
	'word/ID': tf.FixedLenFeature([], tf.string),
	'session/number': tf.FixedLenFeature([], tf.int64),
	'word/remembered/label': tf.FixedLenFeature([], tf.int64),
	'spectrogram/regional': tf.FixedLenFeature([], tf.string)
	}

	# Decode the record read by the reader
	features = tf.parse_single_example(serialized_adni_example, features=feature)

	# Retrieve individual attributes
	subject_id = tf.cast(features['subject/ID'], tf.string)
	word_id = tf.cast(features['word/ID'], tf.string)
	session_number = tf.cast(features['session/number'], tf.int32)
	remembered = tf.cast(features['word/remembered/label'], tf.int32)
	vectorized_features = tf.decode_raw(features['spectrogram/regional'], tf.float64)

	return subject_id, word_id, session_number, remembered, vectorized_features