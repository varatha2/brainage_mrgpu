'''Find the center of sMRI images.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import numpy as np
import tensorflow as tf


def center_image(image, threshold = 100, max_distance = 5):
    '''Find the center of an image.
    Args:
        image: 3D image volume
        threshold: threshold to filter out noises
        max_distance: maximum distance between two adjacent cells, used
		to filter out noises

    Returns:
        center: center of the image
    '''

    brain = np.argwhere(image > threshold)
    size_dimension = brain.shape[0]
    center = np.array([0,0,0])
    for i in range(3):
        dimension = brain[:,i]
        dimension.sort()
        min_dim = dimension[0]
        max_dim = dimension[size_dimension-1]
        for j in range(size_dimension//2-1):
            if dimension[j+1]-dimension[j]>max_distance:
                min_dim = dimension[j+1]
        for j in range(size_dimension//2, size_dimension-1):
            if dimension[j+1]-dimension[j]>max_distance:
                max_dim = dimension[j]
        if max_dim - min_dim > 50:
            center[i] = (max_dim+min_dim)//2
        else:
            center[i] = image.shape[i]//2
    return center

def center_image_tf(image, theshold = 0.1, max_distance = 5):
	'''Find the center of an image tensorflow version.
	Args:
		image: 3D image volume
		threshold: threshold to filter out noises
		max_distance: maximum distance between two adjacent cells, used
		to filter out noises

    Returns:
        center: center of the image
    '''

	brain = tf.where(tf.greater(image, theshold))
	size_dimension = tf.shape(brain)[0]
	center  = []

	for i in range(3):
		dimension = brain[:,i+1]
		dimension = tf.contrib.framework.sort(dimension, axis = -1, direction = 'ASCENDING')
		min_dim = dimension[0]
		max_dim = dimension[size_dimension-1]

		j = tf.Variable(0, tf.int32)
		c1 = lambda j,min_dim: tf.less(j, size_dimension//2 - 1)
		b1 = lambda j,min_dim: (tf.add(j, 1), tf.cond(dimension[j+1]-dimension[j]>max_distance, lambda: dimension[j+1], lambda: min_dim))
		tf.while_loop(c1, b1, (j, min_dim))

		k = size_dimension//2
		c2 = lambda k,max_dim: tf.less(k, size_dimension - 1)
		b2 = lambda k,max_dim: (tf.add(k, 1), tf.cond(dimension[k+1]-dimension[k]>max_distance, lambda: dimension[k], lambda: max_dim))
		tf.while_loop(c2, b2, (k, max_dim))

		center.append(tf.cond(max_dim - min_dim > 50, lambda: tf.cast((max_dim+min_dim)//2, tf.int32), lambda: tf.shape(image)[i]//2))
	return center
