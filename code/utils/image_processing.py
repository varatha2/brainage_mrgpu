'''Image processing utils to pre-process sMRI images.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import numpy as np
import tensorflow as tf


def unit_normalize(image):
	'''Normalizes the image volume values between [0,1].
	Args:
		image: 3D image volume

	Returns:
		image: normalized image
	'''

	image = image - np.amin(image)
	image = image / np.amax(image)

	return image

def unit_normalize_tf(image):
	'''Normalizes tensor values between [0,1] (using tensorflow).
	Args:
		image: 3D-tensor containing image volume

	Returns:
		image: 3D-tensor containing normalized image
	'''

	image = image - tf.reduce_min(image)
	image = image / tf.reduce_max(image)

	return image

def center_crop(image, crop_shape):
	'''Crops the 3D image to the specified shape.
	Args:
		image: 3D image volume
		crop_shape: shape of the cropped image

	Returns:
		image: center-cropped image
	'''

	if len(crop_shape) != 3:
		print('Wrong crop shape')
		return image

	pad_dims = np.zeros((3, 2), dtype=int)

	for i in range(0, 3):
		if(image.shape[i] < crop_shape[i]):
			pad_dims[i, :] = ((crop_shape[i] - image.shape[i]) // 2, (crop_shape[i] - image.shape[i]) // 2)

	image = np.pad(image,
				   ((pad_dims[0, 0], pad_dims[0, 1]),
				   (pad_dims[1, 0], pad_dims[1, 1]),
				   (pad_dims[2, 0], pad_dims[2, 1])),
				   'constant')

	crop_dims = np.zeros((3, 2), dtype=int)

	for i in range(0, 3):
		crop_dims[i, 1] = image.shape[i] - 1
		if (image.shape[i] > crop_shape[i]):
			crop_dims[i, :] = ((image.shape[1] - crop_shape[1]) // 2, (image.shape[1] + crop_shape[1]) // 2)

	image = image[crop_dims[0, 0] : crop_dims[0, 1],
			crop_dims[1, 0] : crop_dims[1, 1],
			crop_dims[2, 0] : crop_dims[2, 1]]

	return image

def center_crop_tf(image, crop_shape):
	'''Crops a 3D image (tensor) to the specified shape around center (using tensorflow).
	Args:
		image: 3D-tensor containing image volume
		crop_shape: shape of the cropped image

	Returns:
		image: 3D-tensor containing center-cropped image
	'''

	# if len(crop_shape) != 3:
	# 	print('Wrong crop shape')
	# 	return image

	# Padding
	pad_dims = tf.nn.relu((crop_shape - tf.shape(image)) // 2)
	pad_dims_odd = tf.where(tf.cast(tf.floormod(crop_shape - tf.shape(image), 2), tf.bool), pad_dims + 1, pad_dims)
	pad_dims = tf.stack([pad_dims, pad_dims_odd], axis=1)
	image = tf.pad(image, pad_dims, 'CONSTANT')

	# Cropping
	begins = tf.nn.relu((tf.shape(image) - crop_shape) // 2)
	crop_shape_new = tf.minimum(tf.shape(image), crop_shape)
	im_cropped = tf.slice(image, begins, crop_shape_new)

	return im_cropped

def center_image(image, threshold = 100, max_distance = 5):
	'''Find the center of an image.
	Args:
		image: 3D image volume
		threshold: threshold to filter out noises
		max_distance: maximum distance between two adjacent cells, used
		to filter out noises

	Returns:
		center: center of the image
	'''

	brain = np.argwhere(image > threshold)
	size_dimension = brain.shape[0]
	center = np.array([0,0,0])
	for i in range(3):
		dimension = brain[:,i]
		dimension.sort()
		min_dim = dimension[0]
		max_dim = dimension[size_dimension-1]
		for j in range(size_dimension//2-1):
			if dimension[j+1]-dimension[j]>max_distance:
				min_dim = dimension[j+1]
		for j in range(size_dimension//2, size_dimension-1):
			if dimension[j+1]-dimension[j]>max_distance:
				max_dim = dimension[j]
		if max_dim - min_dim > 50:
			center[i] = (max_dim+min_dim)//2
		else:
			center[i] = image.shape[i]//2
	return center

def center_image_tf(image, theshold = 0.1, max_distance = 5):
	'''Find the center of an image tensorflow version.
	Args:
		image: 3D image volume
		threshold: threshold to filter out noises
		max_distance: maximum distance between two adjacent cells, used
		to filter out noises

	Returns:
		center: center of the image
	'''

	brain = tf.where(tf.greater(image, theshold))
	size_dimension = tf.shape(brain)[0]
	center  = []

	for i in range(3):
		dimension = brain[:,i+1]
		dimension = tf.contrib.framework.sort(dimension, axis = -1, direction = 'ASCENDING')
		min_dim = dimension[0]
		max_dim = dimension[size_dimension-1]

		j = tf.Variable(0, tf.int32)
		c1 = lambda j,min_dim: tf.less(j, size_dimension//2 - 1)
		b1 = lambda j,min_dim: (tf.add(j, 1), tf.cond(dimension[j+1]-dimension[j]>max_distance, lambda: dimension[j+1], lambda: min_dim))
		tf.while_loop(c1, b1, (j, min_dim))

		k = size_dimension//2
		c2 = lambda k,max_dim: tf.less(k, size_dimension - 1)
		b2 = lambda k,max_dim: (tf.add(k, 1), tf.cond(dimension[k+1]-dimension[k]>max_distance, lambda: dimension[k], lambda: max_dim))
		tf.while_loop(c2, b2, (k, max_dim))

		center.append(tf.cond(max_dim - min_dim > 50, lambda: tf.cast((max_dim+min_dim)//2, tf.int32), lambda: tf.shape(image)[i]//2))
	return center
