'''Utility to read tfrecord files and generate data for training and evaluation.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
from . import adni_tfexample
from . import image_processing


def generate_batch_input(tfrecord_path, label_key, crop_shape, batch_size, capacity, shuffle=True,
                         used_dtype=tf.float32,):
    '''Generates a batch input for given batch size and label key.
    (Should be run within a tf session.)

    Args:
        tfrecord_path: path to the tfrecord file.
        label_key: key of the label that will be returned.
        crop_shape: shape of the image after center cropping.
        batch_size: training batch size.
        capacity: total number of samples.
        shuffle: boolean value indicating whether the queue is shuffled or not.
        used_dtype: the floating point precision to be used

    Returns:
        images: training images.
        labels: training labels.
    '''

    with tf.name_scope('batch_processing'):
        data_files = []
        for path in tfrecord_path:
            data_files.append(tf.gfile.Glob('%s-*' % path))
    if data_files is None:
        raise ValueError('No data files found for this dataset')

    # Create filename queue
    filename_queue = tf.train.string_input_producer(data_files, num_epochs=None)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    # Parse the example
    clinical_stage, age, gender, subject_id, image_id, depth, height, width, vectorized_volume = \
        adni_tfexample.parse_adni_example(serialized_example)

    if label_key == 'AD/clinical_stage/value':
        label = clinical_stage
    elif label_key == 'Subject/age/value':
        label = age
    elif label_key == 'Subject/gender/value':
        label = gender
    else:
        label = age

    # Pre-processing - center crop and unit normalization
    image_orig = tf.reshape(tf.cast(vectorized_volume, used_dtype), [depth, height, width])
    image_cropped = image_processing.center_crop_tf(image_orig, crop_shape)
    image = tf.reshape(image_processing.unit_normalize_tf(image_cropped), [crop_shape[0]*crop_shape[1]*crop_shape[2]])
    label = tf.reshape(tf.cast(label, used_dtype), [1])

    # Creates batches
    if shuffle:
        images, labels = tf.train.shuffle_batch([image, label], batch_size=batch_size, capacity=capacity, num_threads=1,
                                            min_after_dequeue=batch_size, allow_smaller_final_batch=True)
    else:
        images, labels = tf.train.batch([image, label], batch_size=batch_size, capacity=capacity, num_threads=1,
                                                min_after_dequeue=batch_size, allow_smaller_final_batch=True)

    return images, labels


def generate_batch_input_parallel(tfrecord_path, label_key, crop_shape, batch_size, capacity, train,
                                  used_dtype=tf.float32, num_preprocess_threads=None, num_readers=1):
    '''Contruct batches of training or evaluation examples from the image dataset.
    Args:
        tfrecord_path: path to the tfrecord file/s (multiple paths should be inputted as a list).
        label_key: key of the label that will be returned.
        crop_shape: shape of the image after center cropping.
        batch_size: integer
        capacity: total number of samples.
        train: boolean
        used_dtype: the floating point precision to be used
        num_preprocess_threads: integer, total number of preprocessing threads
        num_readers: integer, number of parallel readers
    Returns:
        images: 1-D vectorized Tensor of a batch of images
        labels: 1-D integer Tensor of [batch_size].
    Raises:
        ValueError: if data is not found
    '''

    # # Convert tfrecord_path to list type if not already a list
    # if not isinstance(tfrecord_path, list):
    #     tfrecord_path = [tfrecord_path]

    with tf.name_scope('batch_processing'):
        data_files = []
        for path in tfrecord_path:
            data_files += tf.gfile.Glob('%s-*' % path)
    if data_files is None:
        raise ValueError('No data files found for this dataset')

    # Create filename_queue
    if train:
        filename_queue = tf.train.string_input_producer(data_files,
                                                      shuffle=True,
                                                      capacity=4)
    else:
        filename_queue = tf.train.string_input_producer(data_files,
                                                      shuffle=False,
                                                      capacity=1)
    if num_preprocess_threads is None:
        num_preprocess_threads = 4

    if num_preprocess_threads % 4:
        raise ValueError('Please make num_preprocess_threads a multiple '
                       'of 4 (%d % 4 != 0).', num_preprocess_threads)

    if num_readers is None:
        num_readers = 1

    if num_readers < 1:
        raise ValueError('Please make num_readers at least 1')

    # # Approximate number of examples per shard.
    # examples_per_shard = 50
    # # Size the random shuffle queue to balance between good global
    # # mixing (more examples) and memory use (fewer examples).
    # # 1 image uses 299*299*3*4 bytes = 1MB
    # # The default input_queue_memory_factor is 16 implying a shuffling queue
    # # size: examples_per_shard * 16 * 1MB = 17.6GB
    # min_queue_examples = examples_per_shard * 16
    if train:
        examples_queue = tf.RandomShuffleQueue(
          capacity=capacity,
          min_after_dequeue=batch_size,
          dtypes=[tf.string])
    else:
        examples_queue = tf.FIFOQueue(
          capacity=capacity,
          dtypes=[tf.string])

    # Create multiple readers to populate the queue of examples.
    if num_readers > 1:
        enqueue_ops = []
        for _ in range(num_readers):
            reader = tf.TFRecordReader()
            _, value = reader.read(filename_queue)
            enqueue_ops.append(examples_queue.enqueue([value]))

        tf.train.queue_runner.add_queue_runner(
          tf.train.queue_runner.QueueRunner(examples_queue, enqueue_ops))
        serialized_example = examples_queue.dequeue()
    else:
        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)

    images_and_labels = []
    for thread_id in range(num_preprocess_threads):
        # Parse a serialized Example proto to extract the image and metadata.
        # Parse the example
        clinical_stage, age, gender, subject_id, image_id, depth, height, width, vectorized_volume = \
            adni_tfexample.parse_adni_example(serialized_example)

        if label_key == 'AD/clinical_stage/value':
            label = clinical_stage - 1          # for one-hot conversion values should be in [0,1,2] for depth 3.
        elif label_key == 'Subject/age/value':
            label = age
        elif label_key == 'Subject/gender/value':
            label = gender
        else:
            label = age

        # Pre-processing - center crop and unit normalization
        image_orig = tf.reshape(tf.cast(vectorized_volume, used_dtype), [depth, height, width])
        image_cropped = image_processing.center_crop_tf(image_orig, crop_shape)
        image = tf.reshape(image_processing.unit_normalize_tf(image_cropped),
                           [crop_shape[0] * crop_shape[1] * crop_shape[2]])
        label = tf.reshape(tf.cast(label, used_dtype), [1])
        images_and_labels.append([image, label])

    images, labels = tf.train.batch_join(
        images_and_labels,
        batch_size=batch_size,
        capacity=capacity)

    return images, tf.reshape(labels, [batch_size, 1])