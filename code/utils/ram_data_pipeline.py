'''Utility to read tfrecord files and generate data for training and evaluation.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
from . import ram_tfexample
from . import image_processing

_INPUT_SHAPE = [47, 52]
_NUM_CHANNELS = 6


def generate_batch_input_parallel(tfrecord_path, crop_shape, batch_size, capacity, train, used_dtype=tf.float32,
                                  num_preprocess_threads=None, num_readers=1):
    '''Contruct batches of training or evaluation examples from the image dataset.
    Args:
        tfrecord_path: path to the tfrecord file.
        crop_shape: shape of the image after center cropping.
        batch_size: integer
        capacity: total number of samples.
        train: boolean
        used_dtype: the floating point precision to be used
        num_preprocess_threads: integer, total number of preprocessing threads
        num_readers: integer, number of parallel readers
    Returns:
        images: 1-D vectorized Tensor of a batch of images
        labels: 1-D integer Tensor of [batch_size].
    Raises:
        ValueError: if data is not found
    '''

    with tf.name_scope('batch_processing'):
        data_files = tf.gfile.Glob(tfrecord_path)
    if data_files is None:
        raise ValueError('No data files found for this dataset')

    # Create filename_queue
    if train:
        filename_queue = tf.train.string_input_producer(data_files,
                                                      shuffle=True,
                                                      capacity=4)
    else:
        filename_queue = tf.train.string_input_producer(data_files,
                                                      shuffle=False,
                                                      capacity=1)
    if num_preprocess_threads is None:
        num_preprocess_threads = 4

    if num_preprocess_threads % 4:
        raise ValueError('Please make num_preprocess_threads a multiple '
                       'of 4 (%d % 4 != 0).', num_preprocess_threads)

    if num_readers is None:
        num_readers = 1

    if num_readers < 1:
        raise ValueError('Please make num_readers at least 1')

    # # Approximate number of examples per shard.
    # examples_per_shard = 50
    # # Size the random shuffle queue to balance between good global
    # # mixing (more examples) and memory use (fewer examples).
    # # 1 image uses 299*299*3*4 bytes = 1MB
    # # The default input_queue_memory_factor is 16 implying a shuffling queue
    # # size: examples_per_shard * 16 * 1MB = 17.6GB
    # min_queue_examples = examples_per_shard * 16
    if train:
        examples_queue = tf.RandomShuffleQueue(
          capacity=capacity,
          min_after_dequeue=batch_size,
          dtypes=[tf.string])
    else:
        examples_queue = tf.FIFOQueue(
          capacity=capacity,
          dtypes=[tf.string])

    # Create multiple readers to populate the queue of examples.
    if num_readers > 1:
        enqueue_ops = []
        for _ in range(num_readers):
            reader = tf.TFRecordReader()
            _, value = reader.read(filename_queue)
            enqueue_ops.append(examples_queue.enqueue([value]))

        tf.train.queue_runner.add_queue_runner(
          tf.train.queue_runner.QueueRunner(examples_queue, enqueue_ops))
        serialized_example = examples_queue.dequeue()
    else:
        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)

    images_and_labels = []
    for thread_id in range(num_preprocess_threads):
        # Parse a serialized Example proto to extract the image and metadata.
        # Parse the example
        _, _, _, remembered, vectorized_features = \
            ram_tfexample.parse_tf_example(serialized_example)

        # Pre-processing - center crop and unit normalization
        image_orig = tf.reshape(tf.cast(vectorized_features, used_dtype),
                                [_INPUT_SHAPE[0], _INPUT_SHAPE[1], _NUM_CHANNELS])
        image_cropped = image_processing.center_crop_tf(image_orig, [crop_shape[0], crop_shape[1], _NUM_CHANNELS])
        image = tf.reshape(image_processing.unit_normalize_tf(image_cropped),
                           [crop_shape[0] * crop_shape[1] * _NUM_CHANNELS])
        label = tf.reshape(tf.cast(remembered, used_dtype), [1])
        images_and_labels.append([image, label])

    images, labels = tf.train.batch_join(
        images_and_labels,
        batch_size=batch_size,
        capacity=capacity)

    return images, tf.reshape(labels, [batch_size, 1])