'''Sample age predictor script using the custom cnn implemented in https://arxiv.org/pdf/1808.04362.pdf
   This script utilizes modified source code from https://gitlab.eecs.umich.edu/mld3/brain_age_prediction'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import numpy as np
from sklearn.metrics import roc_auc_score
import tensorflow as tf
from tensorflow import flags
from ...utils import data_pipeline
from ...feature_extractors import mnist_3d
from nets import inception
from tensorflow.contrib import slim 
import os
from .model.buildCustomCNN import customCNN
from .utils.args import *

# Training parameters
learning_rate = 1e-3
epsilon = 1e-1
num_steps = 50000
display_step = 100

# Network parameters
num_classes = 1
dropout = 0.6

########################################################
# Currently tf.float16 functionality is NOT supported. #
########################################################

def main(unused_argv):
	GetArgs()
	used_dtype = tf.float16 if GlobalOpts.use_fp16 else tf.float32

	# Select CPU
	with tf.device('/device:CPU:0'):
		# Data
		# Training batch generation
		# images, labels = data_pipeline.generate_batch_input('code/test_data/test_tfrecords', 'Subject/gender/value',
		# 													[64, 64, 64], 5, 199, True)
		tr_images, tr_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_1.5T_Yr1_all_2mm',
			'Subject/age/value', [GlobalOpts.crop_shape, GlobalOpts.crop_shape, GlobalOpts.crop_shape],
			GlobalOpts.batch_size, 2292, train=True, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)

		# Unflatten training images 
		tr_images = tf.reshape(tr_images, (-1, GlobalOpts.crop_shape, GlobalOpts.crop_shape, GlobalOpts.crop_shape, 1))

		# Testing batch generation
		ts_images, ts_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_BL_3T_all_2mm',
			'Subject/age/value', [GlobalOpts.crop_shape, GlobalOpts.crop_shape, GlobalOpts.crop_shape],
			GlobalOpts.batch_size, 199, train=False, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)
		
		# Unflatten testing images
		ts_images = tf.reshape(ts_images, (-1, GlobalOpts.crop_shape, GlobalOpts.crop_shape, GlobalOpts.crop_shape, 1))

		X = tf.placeholder(used_dtype, [None, GlobalOpts.crop_shape, GlobalOpts.crop_shape, GlobalOpts.crop_shape, 1])
		Y = tf.placeholder(used_dtype, [None, num_classes])

	# Select GPU
	with tf.device('/device:GPU:0'):
		# cnn model parameters
		is_training = tf.placeholder(tf.bool)	# boolean for if the model is training or not
		scale = 1				# amount to scaling image down by
		conv_layers = [8, 16, 32, 64]		# filters per cnn layer
		fully_connected_layers = [1024, 1]	# neurons per fc layer

		# IMPORTANT: There are many optional parameters that I have not yet tested 
		logits = customCNN(X, is_training, scale, conv_layers, fully_connected_layers)
		print('logits shape', logits.shape)

	# Select GPU
	with tf.device('/device:GPU:1'):
		# use mse for regression
		prediction = logits
		loss_op = tf.losses.mean_squared_error(labels=Y, predictions=prediction)
		
		# Define loss and optimizer		
		optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, epsilon=epsilon)
		train_op = optimizer.minimize(loss_op)

		# Evaluate model
		# correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
		# accuracy = tf.reduce_mean(tf.cast(correct_pred, used_dtype))
		# auc_op = tf.metrics.auc(Y, prediction)

	config = tf.ConfigProto(allow_soft_placement=True)
	config.gpu_options.allow_growth = True

	# Start training
	with tf.Session(config=config) as sess:

		# Initialize the variables (i.e. assign their default value)
		init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
		sess.run(init)
		
		# Create a coordinator and run all QueueRunner objects
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		num_test_batches = 199 // GlobalOpts.batch_size

		test_labels = np.zeros((num_test_batches, GlobalOpts.batch_size))
		test_preds = np.zeros((num_test_batches, GlobalOpts.batch_size))
		
		for step in range(1, num_steps):
			# Run optimization op (backprop)
			tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
			# print('Batch input :' + str(tr_imgs.shape))
			sess.run(train_op, feed_dict={X: tr_imgs, Y: tr_lbls, is_training: True})

			# Print information about training progress
			if not step % display_step:
				loss = 0
				for batch in range(0, num_test_batches):
					ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
					mini_loss = sess.run(loss_op, feed_dict={X: ts_imgs, Y: ts_lbls, is_training: False})
					loss += mini_loss

				print("Step " + str(step) + ", Test Loss= " +
					  "{:.4f}".format(loss / num_test_batches))

		print("Optimization Finished!")


		# Final test loss calculation
		loss = 0
		for batch in range(0, num_test_batches):
			ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
			mini_loss = sess.run(loss_op, feed_dict={X: ts_imgs, Y: ts_lbls, is_training: False})
			loss += mini_loss
		
		print('Final test loss= ' + '{:.3f}'.format(loss / num_test_batches))

		# Stop the threads
		coord.request_stop()

		# Wait for threads to stop
		coord.join(threads)
		sess.close()

if __name__ == '__main__':
	tf.app.run()
