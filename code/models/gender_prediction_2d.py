'''Sample gender predictor script based on MNIST example.'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import numpy as np
from sklearn.metrics import roc_auc_score
import tensorflow as tf
from tensorflow import flags
from .utils import data_pipeline
from .feature_extractors import mnist_2d

FLAGS = flags.FLAGS

flags.DEFINE_integer('crop_shape', 128, 'Crop shape of one dimension, the same shape will be used for all dimensions.')
flags.DEFINE_integer('batch_size', 4, 'Number of input images in a batch.')
flags.DEFINE_boolean('use_fp16', False, 'Boolean indicating whether float16 precision is used.')

# Training parameters
learning_rate = 0.001
num_steps = 50000
display_step = 1000

# Network parameters
num_classes = 1
dropout = 0.6

########################################################
# Currently tf.float16 functionality is NOT supported. #
########################################################

def main(unused_argv):

	used_dtype = tf.float16 if FLAGS.use_fp16 else tf.float32

	# Select CPU
	with tf.device('/device:CPU:0'):
		# Data
		# Training batch generation
		# images, labels = data_pipeline.generate_batch_input('code/test_data/test_tfrecords', 'Subject/gender/value',
		# 													[64, 64, 64], 5, 199, True)
		tr_images, tr_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_1.5T_Yr1_all_2mm',
			'Subject/gender/value', [FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape],
			FLAGS.batch_size, 2292, train=True, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)

		tr_images_2d = tr_images[:, :, FLAGS.crop_shape/2]

		# Testing batch generation
		ts_images, ts_labels = data_pipeline.generate_batch_input_parallel(
			'/home/varatha2/projects/data/adni/tf_records/adni_BL_3T_all_2mm',
			'Subject/gender/value', [FLAGS.crop_shape, FLAGS.crop_shape, FLAGS.crop_shape],
			FLAGS.batch_size, 199, train=False, used_dtype=used_dtype,
			num_preprocess_threads=8, num_readers=8)

		ts_images_2d = ts_images[:, :, FLAGS.crop_shape/2]

		X = tf.placeholder(used_dtype, [None, FLAGS.crop_shape * FLAGS.crop_shape])
		Y = tf.placeholder(used_dtype, [None, num_classes])
		keep_prob = tf.placeholder(used_dtype)

	# Computational Graph
	cnet = mnist_2d.MNIST2D([FLAGS.crop_shape, FLAGS.crop_shape], used_dtype)

	# Select CPU
	with tf.device('/device:CPU:0'):
		# Init weights
		(weights, biases) = cnet.init_weights()

	# Select GPU
	with tf.device('/device:GPU:0'):
		# Construct model
		logits = cnet.conv_net(X, weights, biases, keep_prob)

	# Select GPU
	with tf.device('/device:GPU:1'):

		prediction = tf.nn.sigmoid(logits)

		# Define loss and optimizer
		loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
			logits=logits, labels=Y))

		optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, epsilon=1e-4)
		train_op = optimizer.minimize(loss_op)

		# Evaluate model
		# correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
		# accuracy = tf.reduce_mean(tf.cast(correct_pred, used_dtype))
		# auc_op = tf.metrics.auc(Y, prediction)

	config = tf.ConfigProto(allow_soft_placement=True)
	config.gpu_options.allow_growth = True

	# Start training
	with tf.Session(config=config) as sess:

		# Initialize the variables (i.e. assign their default value)
		init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
		# Run the initializer
		sess.run(init)

		# Create a coordinator and run all QueueRunner objects
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		num_test_batches = FLAGS.batch_size // FLAGS.batch_size

		test_labels = np.zeros((num_test_batches, FLAGS.batch_size))
		test_preds = np.zeros((num_test_batches, FLAGS.batch_size))

		for step in range(1, num_steps):
			# Run optimization op (backprop)
			tr_imgs, tr_lbls = sess.run([tr_images, tr_labels])
			# print('Batch input :' + str(tr_imgs.shape))
			sess.run(train_op, feed_dict={X: tr_imgs, Y: tr_lbls, keep_prob: dropout})
			if not step % display_step:
				loss = 0
				for batch in range(0, num_test_batches):
					ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
					mini_loss, pred = sess.run([loss_op, prediction], feed_dict={X: ts_imgs, Y: ts_lbls, keep_prob: 1.0})
					test_labels[batch, :] = np.reshape(ts_lbls, [-1])
					test_preds[batch, :] = np.reshape(pred, [-1])
					loss += mini_loss

				# print("Step " + str(step) + ", Test Loss= " +
				# 	  "{:.4f}".format(loss / num_test_batches) + ", Test Preds= " + str(test_preds))
				print("Step " + str(step) + ", Test Loss= " +
					  "{:.4f}".format(loss / num_test_batches) + ", Test AUC= " + "{:.3f}".format(
					roc_auc_score(np.reshape(test_labels, [-1]), np.reshape(test_preds, [-1]))))

		print("Optimization Finished!")

		# Final test AUC calculation
		for batch in range(0, num_test_batches):
			ts_imgs, ts_lbls = sess.run([ts_images, ts_labels])
			pred = sess.run(prediction, feed_dict={X: ts_imgs, Y: ts_lbls, keep_prob: 1.0})
			test_labels[batch, :] = np.reshape(ts_lbls, [-1])
			test_preds[batch, :] = np.reshape(pred, [-1])

		print("Final test AUC= " + "{:.3f}".format(
			roc_auc_score(np.reshape(test_labels, [-1]), np.reshape(test_preds, [-1]))))

		# Stop the threads
		coord.request_stop()

		# Wait for threads to stop
		coord.join(threads)
		sess.close()

if __name__ == '__main__':
	tf.app.run()
