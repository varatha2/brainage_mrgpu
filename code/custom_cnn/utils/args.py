import argparse

class Options(object):
    pass
GlobalOpts = Options()

def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x

def ParseArgs(description, additionalArgs=[], useDefaults=True):
    parser = argparse.ArgumentParser(description=description)
    required = parser.add_argument_group('required arguments')
    if useDefaults:
        required.add_argument('--gpuMemory', help='A float between 0 and 1. The fraction of available memory to use.', action='store', type=restricted_float, dest='gpuMemory', required=True)
        required.add_argument('--numSteps', help='The number of steps to train for.', action='store', type=int, dest='numSteps', required=True)
    for argDict in additionalArgs:
        if 'default' in argDict:
            required.add_argument(argDict['flag'],
                                  help=argDict['help'],
                                  action=argDict['action'],
                                  type=argDict['type'],
                                  dest=argDict['dest'],
                                  required=argDict['required'],
				  default=argDict['default'],
                                  const=argDict['const'])
        elif 'const' in argDict:
            required.add_argument(argDict['flag'],
                                  help=argDict['help'],
                                  action=argDict['action'],
                                  type=argDict['type'],
                                  dest=argDict['dest'],
                                  required=argDict['required'],
                                  const=argDict['const'])
        else:
            required.add_argument(argDict['flag'],
                                  help=argDict['help'],
                                  action=argDict['action'],
                                  type=argDict['type'],
                                  dest=argDict['dest'],
                                  required=argDict['required'])
    args = parser.parse_args()
    for argDict in additionalArgs:
        setattr(GlobalOpts, argDict['dest'], getattr(args, argDict['dest']))
    if useDefaults:
        GlobalOpts.gpuMemory = args.gpuMemory
        GlobalOpts.numSteps = args.numSteps

def GetArgs():
    additionalArgs = [
	# New args
	{
	'flag': '--crop_shape',
	'help': 'Crop shape of one dimension, the same shape will be used for all dimensions.',
	'action': 'store',
	'type': int,
	'dest': 'crop_shape',
	'default': 128,
	'required': False,
	'const': None
	},
	{
	'flag': '--batch_size',
	'help': 'Number of input images in a batch.',
	'action': 'store',
	'type': int,
	'dest': 'batch_size',
	'default': 4,
	'required': False,
	'const': None
	},
	{
	'flag': '--use_fp16',
	'help': 'Boolean indicating whether float16 precision is used.',
	'action': 'store',
	'type': bool,
	'dest': 'use_fp16',
	'default': False,
	'required': False,
	'const': None
	},


	# Given args
        {
        'flag': '--scale',
        'help': 'The scale at which to slice dimensions. For example, a scale of 2 means that each dimension will be devided into 2 distinct regions, for a total of 8 contiguous chunks.',
        'action': 'store',
        'type': int,
        'dest': 'scale',
        'required': False # True
        },
        {
        'flag': '--type',
        'help': 'One of: traditional, reverse',
        'action': 'store',
        'type': str,
        'dest': 'type',
        'required': False # True
        },
        {
        'flag': '--summaryName',
        'help': 'The file name to put the results of this run into.',
        'action': 'store',
        'type': str,
        'dest': 'summaryName',
        'required': False # True
        },
        {
        'flag': '--data',
        'help': 'One of: PNC, PNC_GENDER, ABIDE1, ABIDE2, ABIDE2_AGE, PAC, PAC_AGE',
        'action': 'store',
        'type': str,
        'dest': 'data',
        'required': False # True
        },
        {
        'flag': '--poolType',
        'help': 'One of MAX, AVG, STRIDED, NONE. Type of pooling layer used inside the network. Default is max pooling.',
        'action': 'store',
        'type': str,
        'dest': 'poolType',
        'required': False,
        'const': None
        },
        {
        'flag': '--sliceIndex',
        'help': 'Set this to an integer to select a single brain region as opposed to concatenating all regions along the depth channel.',
        'action': 'store',
        'type': int,
        'dest': 'sliceIndex',
        'required': False,
        'const': None
        },
        {
        'flag': '--align',
        'help': 'Set to true to align channels.',
        'action': 'store',
        'type': int,
        'dest': 'align',
        'required': False,
        'const': None
        },
        {
        'flag': '--numberTrials',
        'help': 'Number of repeated models to run.',
        'action': 'store',
        'type': int,
        'dest': 'numberTrials',
        'required': False,
        'const': None
        },
        {
        'flag': '--padding',
        'help': 'Set this to an integer to crop the image to the brain and then apply `padding` amount of padding.',
        'action': 'store',
        'type': int,
        'dest': 'padding',
        'required': False,
        'const': None
        },
        {
        'flag': '--batchSize',
        'help': 'Batch size to train with. Default is 4.',
        'action': 'store',
        'type': int,
        'dest': 'batchSize',
        'required': False,
        'const': None
        },
        {
        'flag': '--pheno',
        'help': 'Specify 1 to add phenotypics to the model.',
        'action': 'store',
        'type': int,
        'dest': 'pheno',
        'required': False,
        'const': None
        },
        {
        'flag': '--validationDir',
        'help': 'Checkpoint directory to restore the model from.',
        'action': 'store',
        'type': str,
        'dest': 'validationDir',
        'required': False,
        'const': None
        },
        {
        'flag': '--regStrength',
        'help': 'Lambda value for L2 regularization. If not specified, no regularization is applied.',
        'action': 'store',
        'type': float,
        'dest': 'regStrength',
        'required': False,
        'const': None
        },
        {
        'flag': '--learningRate',
        'help': 'Global optimization learning rate. Default is 0.0001.',
        'action': 'store',
        'type': float,
        'dest': 'learningRate',
        'required': False,
        'const': None
        },
        {
        'flag': '--maxNorm',
        'help': 'Specify an integer to constrain kernels with a maximum norm.',
        'action': 'store',
        'type': int,
        'dest': 'maxNorm',
        'required': False,
        'const': None
        },
        {
        'flag': '--dropout',
        'help': 'The probability of keeping a neuron alive during training. Defaults to 0.6.',
        'action': 'store',
        'type': float,
        'dest': 'dropout',
        'required': False,
        'const': None
        },
        {
        'flag': '--dataScale',
        'help': 'The downsampling rate of the data. Either 1, 2 or 3. Defaults to 3. ',
        'action': 'store',
        'type': int,
        'dest': 'dataScale',
        'required': False,
        'const': None
        },
        {
        'flag': '--pncDataType',
        'help': 'One of AVG, MAX, NAIVE, POOL_MIX, COMBINE. Defaults to AVG. If set, dataScale cannot be specified.',
        'action': 'store',
        'type': str,
        'dest': 'pncDataType',
        'required': False,
        'const': None
        },
        {
        'flag': '--listType',
        'help': 'Only valid for ABIDE and ADHD. One of strat or site.',
        'action': 'store',
        'type': str,
        'dest': 'listType',
        'required': False,
        'const': None
        },
        {
        'flag': '--depthwise',
        'help': 'Set to 1 to use depthwise convolutions for the entire network.',
        'action': 'store',
        'type': int,
        'dest': 'depthwise',
        'required': False,
        'const': None
        },
        {
        'flag': '--hiddenUnits',
        'help': 'The number of hidden units. Defaults to 256.',
        'action': 'store',
        'type': int,
        'dest': 'hiddenUnits',
        'required': False,
        'const': None
        },
        {
        'flag': '--trainingSize',
        'help': 'Number of training examples. Default is 524, which is max.',
        'action': 'store',
        'type': int,
        'dest': 'trainingSize',
        'required': False,
        'const': None
        },
        {
        'flag': '--skipConnection',
        'help': 'Set to 1 to allow skip connection layer, add residuals to the network (like ResNet).',
        'action': 'store',
        'type': int,
        'dest': 'skipConnection',
        'required': False,
        'const': None
        },
        {
        'flag': '--maxRatio',
        'help': 'Ratio of max pooling in the pool_mix augmentation. Default to 0.25.',
        'action': 'store',
        'type': float,
        'dest': 'maxRatio',
        'required': False,
        'const': None
        },
        {
        'flag': '--augRatio',
        'help': 'Ratio of augmented images versus pure average images in the pool_mix and combine augmentation. Default to 2.',
        'action': 'store',
        'type': float,
        'dest': 'augRatio',
        'required': False,
        'const': None
        },
        {
        'flag': '--testType',
        'help': 'One of AVG, MAX. Type of validation and test file preprocessing setting. Default to AVG.',
        'action': 'store',
        'type': str,
        'dest': 'testType',
        'required': False,
        'const': None
        },
        {
        'flag': '--augment',
        'help': 'One of FLIP, TRANSLATE. Type of standard augmentation. Default to None.',
        'action': 'store',
        'type': str,
        'dest': 'augment',
        'required': False,
        'const': None
        }
        ]
    ParseArgs('Run 3D CNN over structural MRI volumes', additionalArgs=additionalArgs, useDefaults=False)
    if GlobalOpts.numberTrials is None:
        GlobalOpts.numberTrials = 5
    if GlobalOpts.batchSize is None:
        GlobalOpts.batchSize = 4
    if GlobalOpts.learningRate is None:
        GlobalOpts.learningRate = 0.0001
    if GlobalOpts.dropout is None:
        GlobalOpts.dropout = 0.6
    if GlobalOpts.dataScale is None:
        GlobalOpts.dataScale = 3
    if GlobalOpts.pncDataType is None:
        GlobalOpts.pncDataType = 'AVG'
    if GlobalOpts.listType is None:
        GlobalOpts.listType = 'strat'
    if GlobalOpts.hiddenUnits is None:
        GlobalOpts.hiddenUnits = 256
    if GlobalOpts.poolType is None:
        GlobalOpts.poolType = 'MAX'
    if GlobalOpts.pncDataType == 'POOL_MIX' and GlobalOpts.maxRatio is None:
        GlobalOpts.maxRatio = 0.25
    if GlobalOpts.pncDataType == 'POOL_MIX' and GlobalOpts.augRatio is None:
        GlobalOpts.augRatio = 2


