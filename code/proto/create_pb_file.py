from . import pipeline_pb2
from google.protobuf import text_format


pl = pipeline_pb2.Pipeline()
pl.head.classification.num_classes = 2
pl.head.classification.label = 'Subject/gender/value'
pl.feature_extractor.conv_net = 0
pl.preprocessing.crop.extend([64,64,64])
pl.preprocessing.zoom.extend([2,2,2])
pl.training.data.extend(['/home/varatha2/projects/data/HCP/tf_records/hcp_all_2mm',
				   '/home/varatha2/projects/data/IXI/tf_records/ixi_all_2mm',
				   '/home/varatha2/projects/data/adni/tf_records/adni_1.5T_Yr1_all_2mm'])
pl.training.steps = 1000
pl.training.adam.learning_rate = 1e-5
pl.training.adam.epsilon = 1e-4
pl.training.num_examples = 1113 + 563 + 2292
pl.testing.data.extend(['/home/varatha2/projects/data/adni/tf_records/adni_BL_3T_all_2mm'])
pl.testing.num_examples = 199
pl.batch_size = 8
pl.metrics.extend(['accuracy', 'auc'])
pl.experiment_basedir = '/home/varatha2/projects/data/experiments/inception_v1_gender/'
with open('code/proto/pipeline.txt', 'w') as f:
	f.write(text_format.MessageToString(pl))
