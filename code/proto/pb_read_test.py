from . import pipeline_pb2
from google.protobuf import text_format
import os

def main(unused_argv):
	pl = pipeline_pb2.Pipeline()
	with open('code/proto/pipeline.txt', 'r') as f:
		text_format.Parse(f.read(), pl)
	print(pl)
	if pl.head.classification:
		# subprocess.Popen("python -m code.models.gender_predictor --crop_shape=" + str(pl.preprocessing.crop[0]), shell = True)
		# os.system("python -m code.models.gender_predictor --crop_shape=" + str(pl.preprocessing.crop[0]))
		print('Classification head..')

if __name__ == '__main__':
	main(0)
