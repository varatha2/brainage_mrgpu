'''
Python script to load and ingest sMRI volumes as tensorflow examples.

To run this (from ~/Desktop/brain_age/):

python -m code.ingestion.ixi_ingestion \
--data_dir='code/IXI/' \
--outfile_name='code/IXI/IXI_tfrecords' \
--input_csv='code/IXI/IXI_labels.csv'

'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import logging
import numpy as np
import pandas as pd
import os
import re
from joblib import Parallel
from scipy.ndimage import zoom
import sys
import tensorflow as tf
from tensorflow import gfile
from tensorflow import flags
from ..utils import medical_volume as medvol
from ..utils import ixi_tfexample
from ..utils import image_processing
import nibabel as nib
from  scipy.ndimage.interpolation import rotate

FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where images are located')
flags.DEFINE_string('outfile_name', 'test_tfrecords', 'Output file name including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')

_GENDER_NUMERIC_MAP = {
    2: 0,
    1: 1
}  # A mapping from string gender label to numerics


def write_tfrecord(img_names, outfile_name, input_csv):
	''' This method writes the images given in img_names as TF Records.

	Args:
		img_names: list containing image names.
		outfile_name: name of the output TF Record file.
		input_csv: name of the CSV file containing label information.'''

	# open the TFRecords file.
	writer = tf.python_io.TFRecordWriter(outfile_name)

	for i, image_name in enumerate(img_names):

		# Find image id from image name.
		match = re.search(r'IXI'+'\d*?'+'-', image_name)
		image_id = match.group()[+3:-1]

		if not (gfile.Exists(input_csv)):
			return

		label_csv = pd.read_csv(gfile.Open(input_csv))

		# Extract labels from CSV file.
		if len(label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'AGE'].values) == 0:
			if len(label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'DOB'].values) == 0:
				continue
			dob_string = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'DOB'].values[0]
			image_aq_yr = 2006 if int(image_id) > 232 else 2005
			age = image_aq_yr - (datetime.strptime(dob_string, "%Y-%m-%d")).year
		else:
			age = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'AGE'].values[0]
		gender = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'SEX_ID (1=m, 2=f)'].map(_GENDER_NUMERIC_MAP).values[0]
		subject_id = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'IXI_ID'].values[0]

		# Load image.
		mv = medvol.MedicalVolume()
		if mv.load_nifti_volume_nib(image_name):
			img = mv.data

			# Resize image to the actual physical dimension.
			img = zoom(img, (mv.pixdims[0], mv.pixdims[1], mv.pixdims[2]))

			img = rotate(img, 90, [1, 0])

			# img = image_processing.unit_normalize(img)
			#
			# # Crop the image to [64,64,64].
			# img = image_processing.center_crop(img, (64, 64, 64))

			img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2], -1)

			example = ixi_tfexample.create_IXI_example(
				age=age,
				gender=gender,
				subject_id=subject_id,
				image_id=image_id,
				depth=img.shape[1],
				height=img.shape[2],
				width=img.shape[3],
				vectorized_volume=img.reshape(-1)
				)
			print(img.shape)

			print('Exporting image ' + image_id)

			# Serialize to string and write on the file.
			writer.write(example.SerializeToString())
		else:
			print('Could not load file ' + image_name)

	writer.close()
	sys.stdout.flush()


def main(unused_argv):

	all_files = glob.glob(os.path.join(os.path.abspath(FLAGS.data_dir), '*.nii.gz'))
	write_tfrecord(all_files, os.path.abspath(FLAGS.outfile_name), os.path.abspath(FLAGS.input_csv))


if __name__ == '__main__':
	tf.app.run()
