'''
Python script to load and ingest sMRI volumes as tensorflow examples.

Run (from base directory ../brain_age/):
python -m code.ingestion.ram_ingestion_parallel \
--data_dir='/home/varatha2/projects/data/RAM/Matlab/Dataset/' \
--outfile_name='/home/varatha2/projects/data/RAM/tfrecords/RAM-tfrecords-6-regions-patient-based' \
--input_csv='/home/varatha2/projects/data/RAM/Matlab/Dataset/patwordsheet.csv'
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import numpy as np
import pandas as pd
from datetime import datetime
import os
from scipy.io import loadmat
import sys
import tensorflow as tf
from tensorflow.app import flags
import threading
from ..utils import ram_tfexample


FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where the mat files are located in respective subdirectories')
flags.DEFINE_string('outfile_name', 'test_tfrecords', 'Output file name including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')


# The brain regions that are considered in this project
_REGIONS = ['FrontalLobe', 'LimbicLobe', 'OccipitalLobe', 'ParietalLobe', 'Sublobar', 'TemporalLobe']


def _process_image_files_batch(thread_index, ranges, outfile_name, input_csv, num_shards):
	"""Processes and saves list of images as TFRecord in 1 thread.
	Args:
	thread_index: integer, unique batch to run index is within [0, len(ranges)).
	ranges: list of pairs of integers specifying ranges of each batches to
	  analyze in parallel.
	outfile_name: string, unique identifier specifying the output file name
	input_csv: absolute path to the CSV file containing label information.
	num_shards: integer number of shards for this data set.
	"""
	# Each thread produces N shards where N = int(num_shards / num_threads).
	# For instance, if num_shards = 128, and the num_threads = 2, then the first
	# thread would produce shards [0, 64).
	num_threads = len(ranges)
	assert not num_shards % num_threads
	num_shards_per_batch = int(num_shards / num_threads)

	shard_ranges = np.linspace(ranges[thread_index][0],
							 ranges[thread_index][1],
							 num_shards_per_batch + 1).astype(int)
	num_files_in_thread = ranges[thread_index][1] - ranges[thread_index][0]

	# Read CSV file and replace nans with empty strings
	word_table = pd.read_csv(input_csv).replace(np.nan, '', regex=True)

	counter = 0
	for s in range(num_shards_per_batch):
		# Generate a sharded version of the file name, e.g. 'train-00002-of-00010'
		shard = thread_index * num_shards_per_batch + s
		output_filename = '%s-%.5d-of-%.5d' % (outfile_name, shard, num_shards)
		writer = tf.python_io.TFRecordWriter(output_filename)

		shard_counter = 0
		files_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
		for i in files_in_shard:
			# Extract subject ID, word number, session number, and the class label from CSV file
			pt_wd_id = word_table['PtWdID'][i]
			subject_id = pt_wd_id[:-5]
			word_id = pt_wd_id[-5:]
			session_number = word_table['Session'][i]
			remembered = word_table['Label'][i]

			# Extract regional spectrogram features from the mat files
			regional_specgrms = np.zeros((47, 52, len(_REGIONS)))

			# print(str(thread_index) + ':' + pt_wd_id)

			for reg in range(len(_REGIONS)):
				# print(word_table[_REGIONS[reg]][i])
				regional_electrodes = word_table[_REGIONS[reg]][i]
				all_mats = regional_electrodes.split(';')
				for each in all_mats:
					if not each == '':
						spec_mat = loadmat(os.path.join(FLAGS.data_dir, each[1:]))
						# All elctrodes in a region are averaged to get a regional spectrogram
						regional_specgrms[:, :, reg] = spec_mat['wdspectrogram'] / len(all_mats)


			example = ram_tfexample.create_tf_example(
				subject_id=subject_id,
				word_id=word_id,
				session_number=session_number,
				remembered=remembered,
				vectorized_features=regional_specgrms.reshape(-1)
			)

			writer.write(example.SerializeToString())
			shard_counter += 1
			counter += 1

			if not counter % 50:
				print('%s [thread %d]: Processed %d of %d images in thread batch.' %
					  (datetime.now(), thread_index, counter, num_files_in_thread))
			sys.stdout.flush()

		writer.close()
		print('%s [thread %d]: Wrote %d images to %s' %
			  (datetime.now(), thread_index, shard_counter, output_filename))
		sys.stdout.flush()

		print('%s [thread %d]: Wrote %d images to %d shards.' %
			(datetime.now(), thread_index, counter, num_files_in_thread))
		sys.stdout.flush()


def _process_image_files(outfile_name, input_csv):
	"""Process and save list of images as TFRecord of Example protos.
	Args:
	outfile_name: string, unique identifier specifying the output file name
	input_csv: name of the CSV file containing label information.
	"""

	# Read input CSV and replace nan values with empty strings
	word_table = pd.read_csv(input_csv)
	num_words = len(word_table)

	patient_ids = word_table['PtWdID'].str.split('_').str[0]
	unique_patient_ids = list(set(patient_ids))

	ranges = []
	for id in unique_patient_ids:
		indices = [i for i, x in enumerate(patient_ids) if x == id]
		ranges.append([indices[0], indices[-1]])

	num_threads = len(unique_patient_ids)
	num_shards = num_threads

	# Launch a thread for each batch.
	print('Launching %d threads for spacings: %s' % (num_threads, ranges))
	sys.stdout.flush()

	# Create a mechanism for monitoring when all threads are finished.
	coord = tf.train.Coordinator()

	threads = []
	for thread_index in range(len(ranges)):
		args = (thread_index, ranges, outfile_name, input_csv, num_shards)
		t = threading.Thread(target=_process_image_files_batch, args=args)
		t.start()
		threads.append(t)

	# Wait for all the threads to terminate.
	coord.join(threads)
	print('%s: Finished writing all %d images in data set.' %
		(datetime.now(), num_words))
	sys.stdout.flush()


def main(unused_argv):
	_process_image_files(FLAGS.outfile_name, FLAGS.input_csv)


if __name__ == '__main__':
	tf.app.run()
