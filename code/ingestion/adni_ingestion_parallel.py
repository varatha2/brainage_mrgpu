'''
Python script to load and ingest sMRI volumes as tensorflow examples.

Test run (from base directory ../brain_age/):
python -m code.ingestion.adni_ingestion_parallel \
--data_dir='code/test_data/' \
--outfile_name='code/test_data/test_tfrecords' \
--input_csv='code/test_data/test_labels.csv' \
--num_threads=2 \
--num_shards=2

Real example (from base directory ../brain_age/):
python -m code.ingestion.adni_ingestion_parallel \
--data_dir='/home/varatha2/projects/data/adni/ADNI1_1_5T_ALL/' \
--outfile_name='/home/varatha2/projects/data/adni/tf_records/adni_1.5T_Yr1_all_2mm' \
--input_csv='/home/varatha2/projects/data/adni/csv_files/ADNI1_Complete_1Yr_1.5T_9_20_2018.csv' \
--voxel_size=2.0 \
--num_threads=20 \
--num_shards=20
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import numpy as np
import pandas as pd
from datetime import datetime
import os
import re
from scipy.ndimage import zoom
import sys
import tensorflow as tf
from tensorflow import gfile
from tensorflow.app import flags
import threading
from ..utils import medical_volume as medvol
from ..utils import adni_tfexample
from ..utils import image_processing

FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where images are located')
flags.DEFINE_string('outfile_name', 'test_tfrecords', 'Output file name including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')
flags.DEFINE_float('voxel_size', 1.0, 'Physical size (in mm) of a voxel in the final saved images.')
flags.DEFINE_integer('num_shards', 2, 'Number of shards in TFRecord files.')
flags.DEFINE_integer('num_threads', 2, 'Number of threads to preprocess the images.')

_CLINICAL_STAGE_NUMERIC_MAP = {
	'CN': 1,  # Cognitively Normal
	'MCI': 2,  # Mild Cognitive Impairment
	'AD': 3  # Alzheimer's Disease
}  # A mapping from string clinical stages to numerics

_GENDER_NUMERIC_MAP = {
	'F': 0,
	'M': 1
}  # A mapping from string gender label to numerics


def _process_image_files_batch(thread_index, ranges, outfile_name, img_names, input_csv, num_shards):
	"""Processes and saves list of images as TFRecord in 1 thread.
	Args:
	thread_index: integer, unique batch to run index is within [0, len(ranges)).
	ranges: list of pairs of integers specifying ranges of each batches to
	  analyze in parallel.
	outfile_name: string, unique identifier specifying the output file name
	img_names: list of strings; each string is a path to an image file
	input_csv: name of the CSV file containing label information.
	num_shards: integer number of shards for this data set.
	"""
	# Each thread produces N shards where N = int(num_shards / num_threads).
	# For instance, if num_shards = 128, and the num_threads = 2, then the first
	# thread would produce shards [0, 64).
	num_threads = len(ranges)
	assert not num_shards % num_threads
	num_shards_per_batch = int(num_shards / num_threads)

	shard_ranges = np.linspace(ranges[thread_index][0],
							 ranges[thread_index][1],
							 num_shards_per_batch + 1).astype(int)
	num_files_in_thread = ranges[thread_index][1] - ranges[thread_index][0]

	counter = 0
	for s in range(num_shards_per_batch):
		# Generate a sharded version of the file name, e.g. 'train-00002-of-00010'
		shard = thread_index * num_shards_per_batch + s
		output_filename = '%s-%.5d-of-%.5d' % (outfile_name, shard, num_shards)
		writer = tf.python_io.TFRecordWriter(output_filename)

		shard_counter = 0
		files_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
		for i in files_in_shard:
			image_name = img_names[i]

			# Find image id from image name.
			match = re.search(r'\d+.nii$', image_name)
			image_id = match.group()[:-4]

			if not (gfile.Exists(input_csv)):
				return

			label_csv = pd.read_csv(gfile.Open(input_csv))

			# Extract labels from CSV file.
			clinical_stage = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Group'].map(_CLINICAL_STAGE_NUMERIC_MAP).values[0]
			age = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Age'].values[0]
			gender = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Sex'].map(_GENDER_NUMERIC_MAP).values[0]
			subject_id = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Subject'].values[0]

			# Load image.
			mv = medvol.MedicalVolume()
			mv.load_nifti_volume_nib(image_name)
			img = mv.data

			# Resize image to the actual physical dimension.
			img = zoom(img, (mv.pixdims[0] / FLAGS.voxel_size, mv.pixdims[1] / FLAGS.voxel_size,
							 mv.pixdims[2] / FLAGS.voxel_size))

			# img = image_processing.unit_normalize(img)
			#
			# # Crop the image to [64,64,64].
			# img = image_processing.center_crop(img, (64, 64, 64))

			img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2], -1)

			example = adni_tfexample.create_adni_example(
				clinical_stage=clinical_stage,
				age=age,
				gender=gender,
				subject_id=subject_id,
				image_id=image_id,
				depth=img.shape[1],
				height=img.shape[2],
				width=img.shape[3],
				vectorized_volume=img.reshape(-1)
			)

			writer.write(example.SerializeToString())
			shard_counter += 1
			counter += 1

			if not counter % 10:
				print('%s [thread %d]: Processed %d of %d images in thread batch.' %
					  (datetime.now(), thread_index, counter, num_files_in_thread))
			sys.stdout.flush()

		writer.close()
		print('%s [thread %d]: Wrote %d images to %s' %
			  (datetime.now(), thread_index, shard_counter, output_filename))
		sys.stdout.flush()

		print('%s [thread %d]: Wrote %d images to %d shards.' %
			(datetime.now(), thread_index, counter, num_files_in_thread))
		sys.stdout.flush()


def _process_image_files(outfile_name, image_names, input_csv, num_threads, num_shards):
	"""Process and save list of images as TFRecord of Example protos.
	Args:
	outfile_name: string, unique identifier specifying the output file name
	img_names: list of strings; each string is a path to an image file
	input_csv: name of the CSV file containing label information.
	num_threads: integer number of threads to preprocess the images.
	num_shards: integer number of shards for this data set.
	"""

	# Break all images into batches with a [ranges[i][0], ranges[i][1]].
	spacing = np.linspace(0, len(image_names), num_threads + 1).astype(np.int)
	ranges = []
	for i in range(len(spacing) - 1):
		ranges.append([spacing[i], spacing[i+1]])

	# Launch a thread for each batch.
	print('Launching %d threads for spacings: %s' % (num_threads, ranges))
	sys.stdout.flush()

	# Create a mechanism for monitoring when all threads are finished.
	coord = tf.train.Coordinator()

	threads = []
	for thread_index in range(len(ranges)):
		args = (thread_index, ranges, outfile_name, image_names, input_csv, num_shards)
		t = threading.Thread(target=_process_image_files_batch, args=args)
		t.start()
		threads.append(t)

	# Wait for all the threads to terminate.
	coord.join(threads)
	print('%s: Finished writing all %d images in data set.' %
		(datetime.now(), len(image_names)))
	sys.stdout.flush()


def main(unused_argv):

	all_files = glob.glob(os.path.join(FLAGS.data_dir, '*.nii'))
	_process_image_files(FLAGS.outfile_name, all_files, FLAGS.input_csv, FLAGS.num_threads, FLAGS.num_shards)


if __name__ == '__main__':
	tf.app.run()